import { AfterViewInit, Component, ElementRef, NgZone, ViewChild, ViewEncapsulation } from '@angular/core';
import { getElementNode, getRange, isArrowKeyEvent, isIeBrowser, isRightKey } from './tools/helpers';
import { alignPosition, fontColors, fontFamily, fontSize } from './tools/constants';

interface IEditorExtraStyle {
    color: string | null;
    fontSize: string | null;
    fontFamily: string | null;
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent implements AfterViewInit {

    @ViewChild('editor')
    public editor: ElementRef;

    public readonly alignPosition: Array<any> = alignPosition;

    public readonly colors: Array<any> = fontColors;

    public readonly fontSizes: Array<any> = fontSize;

    public readonly fontFamily: Array<any> = fontFamily;

    public currentColor!: string;

    public currentFont!: string;

    public currentFontSize!: string;

    public lastRange!: Range;

    public addLinkMode: boolean = false;

    private readonly isIE: boolean = isIeBrowser();

    constructor(
        private zone: NgZone
    ) {}

    public ngAfterViewInit(): void {
        document.execCommand('defaultParagraphSeparator', false, 'p');

        this.zone.runOutsideAngular(() => {
            this.editor.nativeElement.addEventListener('keydown', this.keydownHandler);
            this.editor.nativeElement.addEventListener('mouseup', this.mouseupHandler);
        });
    }

    public editorPasteHandler(event: ClipboardEvent): void {
        event.preventDefault();

        const clipboardData = event.clipboardData || (<any>window).clipboardData;
        const pastedData = clipboardData.getData('Text').replace(/(\r\n|\n|\r)/igm, '').trim();

        const range = getRange();
        const selecton = getSelection();
        const startNode = getElementNode(range.startContainer);
        const editableElem = this.getClosestEditableElement(startNode);

        if (range.collapsed && editableElem) {
            let nextText = startNode.nextSibling;

            if (!nextText) {
                return;
            }

            while (nextText.nodeType !== Node.TEXT_NODE) {
                nextText = <HTMLElement>nextText.firstChild;
            }

            nextText.textContent = pastedData + nextText.textContent;

            range.setStart(startNode.nextSibling, 1);
            event.preventDefault();
        } else {
            if (this.isIE) {
                document.execCommand('paste', false, pastedData);
            } else {
                document.execCommand('insertText', false, pastedData);
            }
        }
    }

    public execCommand(propertyName: string, value?: string): void {
        value
            ? document.execCommand(propertyName, false, value)
            : document.execCommand(propertyName, false)
        ;
    }

    public addUrl(url: string): void {
        if (!this.lastRange) {
            return;
        }

        const selection = getSelection();

        selection.removeAllRanges();

        selection.addRange(this.lastRange);
        this.execCommand('createLink', url);

        this.addLinkMode = false;
    }

    public addDynamicTag(): void {
        const range = getRange();

        if (!this.editor || !this.isRangeInsideEditor()) {
            return;
        }

        const { startContainer, collapsed, startOffset } = range;

        const editableElement = this.getClosestEditableElement(getElementNode(startContainer));


        if (!collapsed) {
            document.execCommand('delete');
        }

        const newNode = document.createElement('span');
        const tag = '<distribution link>';

        newNode.classList.add('contenteditable');
        newNode.setAttribute('tag-length', '' + tag.length);

        newNode.innerText = tag;

        if (collapsed && editableElement) {
            const editableParent = editableElement.parentNode;

            if (startOffset === 0) {
                editableParent.insertBefore(newNode, editableElement);
                range.collapse(true);
            } else {

                if (editableElement.nextSibling) {
                    editableParent.insertBefore(newNode, editableElement.nextSibling);
                } else {
                    editableParent.appendChild(newNode);
                }

                range.collapse(true);
            }

        } else {
            range.insertNode(newNode);
            range.collapse(true);
        }
    }

    public saveCurrentRange(): void {
        this.lastRange = getRange();

        if (this.isRangeInsideEditor() && !this.lastRange.collapsed) {
            this.addLinkMode = true;
        }
    }

    // Method keydownHandler runs outside the angular zone
    private keydownHandler = (e: KeyboardEvent) => {
        if (isArrowKeyEvent(e)) {
            this.zone.runOutsideAngular(() => {
                setTimeout(() => {
                    if (getRange().collapsed) {
                        this.singleRangeHandler(e);
                    } else {
                        this.multiRangeHandler(e);
                    }
                });
            });
        }

        switch (e.key) {
            case 'Backspace':
                this.backspaceHandler(e);

                return;

            case 'Delete':
                this.deleteHandler(e);

                return;
        }

        this.dynamicTagInputHandler(e);
    }

    // Method deleteHandler runs outside the angular zone
    private deleteHandler(event: KeyboardEvent): void {
        const range = getRange();

        if (!range.collapsed) {
            return;
        }

        const { startContainer } = range;
        let startNode;

        if (range.startOffset === 0
            && (
                (<HTMLElement>getElementNode(startContainer)).closest('.contenteditable')
                || (<HTMLElement>getElementNode(startContainer)).classList.contains('contenteditable')
            )
        ) {
            startNode = getElementNode(startContainer);
        } else if (startContainer.textContent.length !== range.startOffset) {
            return;
        } else {
            startNode = getElementNode(startContainer.nextSibling);
        }

        if (!startNode) {
            return;
        }

        const tagElement = this.getClosestEditableElement(startNode);
        const tagLength = tagElement ? parseFloat(tagElement.getAttribute('tag-length')) : null;

        let textNode = startNode;

        while (textNode.nodeType !== Node.TEXT_NODE) {
            textNode = <HTMLElement>textNode.firstChild;
        }

        if (tagElement) {
            const newRange = document.createRange();

            newRange.setStart(textNode, 0);
            newRange.setEnd(textNode, textNode.length < tagLength ? textNode.length : tagLength);

            document.getSelection().removeAllRanges();
            document.getSelection().addRange(newRange);

            document.execCommand('delete', false);
            event.preventDefault();
        }
    }

    // Method backspaceHandler runs outside the angular zone
    private backspaceHandler(event: KeyboardEvent): void {
        if (!getRange().collapsed) {
            return;
        }

        const range = getRange();
        const {startContainer} = range;
        const startNode = <HTMLElement>getElementNode(startContainer);

        const tagElement = this.getClosestEditableElement(startNode);

        const tagLength = tagElement ? parseFloat(tagElement.getAttribute('tag-length')) : null;

        if (tagLength) {
            if (range.endOffset <= tagLength) {
                range.setStart(startContainer, 0);
                document.execCommand('delete', false);
                event.preventDefault();
            }
        }
    }

    // Method singleRangeHandler runs outside the angular zone
    private singleRangeHandler(e: KeyboardEvent): void {
        const range = getRange();
        const {startContainer} = range;
        const startNode = <HTMLElement>getElementNode(startContainer);

        const tagElement = this.getClosestEditableElement(startNode);

        const tagLength = tagElement ? parseFloat(tagElement.getAttribute('tag-length')) : null;

        if (tagLength) {
            if (isRightKey(e.key)) {
                range.setStartAfter(startContainer);
                range.collapse();
            } else {
                if (range.endOffset < tagLength) {
                    range.setStart(startContainer, 0);
                    range.collapse(true);
                }
            }

            e.preventDefault();
        }

        this.findCommonStyle();
    }

    // Method multiRangeHandler runs outside the angular zone
    private multiRangeHandler(e: KeyboardEvent): void {
        const range = getRange();
        const {startContainer, endContainer, startOffset, endOffset} = range;
        const startNode = <HTMLElement>getElementNode(startContainer);
        const endNode = <HTMLElement>getElementNode(endContainer);

        const startTagElement = this.getClosestEditableElement(startNode);
        const endTagElement = this.getClosestEditableElement(endNode);

        const startTagLength = startTagElement ? parseFloat(startTagElement.getAttribute('tag-length')) : null;
        const endTagLength = endTagElement ? parseFloat(endTagElement.getAttribute('tag-length')) : null;

        const selection = getSelection();
        const selRange = getRange().cloneRange();

        if (startTagLength
            && startContainer === selection.focusNode
            && (startOffset > 0 && startOffset < +startTagLength)
        ) {
            selRange.collapse();

            selection.removeAllRanges();
            selection.addRange(selRange);
            selection.extend(range.startContainer, isRightKey(e.key) ? +startTagLength : 0);
        }

        if (endTagLength
            && endContainer === selection.focusNode
            && (endOffset > 0 && endOffset < +endTagLength)
        ) {
            selRange.collapse(true);

            selection.removeAllRanges();
            selection.addRange(selRange);
            selection.extend(range.endContainer, isRightKey(e.key) ? +endTagLength : 0);
        }

        this.findRangeCommonStyle();
    }

    // Method dynamicTagInputHandler runs outside the angular zone
    private dynamicTagInputHandler(e: KeyboardEvent): void {
        if (!isArrowKeyEvent(e) && !e.ctrlKey && e.key.length === 1) {
            const range = getRange();

            const { startContainer } = range;
            const startNode = <HTMLElement>getElementNode(startContainer);
            const editableElement = this.getClosestEditableElement(startNode);

            if (editableElement) {
                const editableParent = editableElement.parentNode;
                const newNode = document.createTextNode(e.key);

                if (range.startOffset !== 0) {
                    if (editableElement.nextSibling) {
                        editableParent.insertBefore(newNode, editableElement.nextSibling);
                    } else {
                        editableParent.appendChild(newNode);
                    }

                    range.setStart(newNode, 1);
                    range.collapse(true);
                } else {
                    editableParent.insertBefore(newNode, editableElement);
                    range.setStart(newNode, 1);
                    range.collapse(true);
                }

                e.preventDefault();
            }
        }
    }

    // Method mouseupHandler runs outside the angular zone
    private mouseupHandler = (): void => {
        this.zone.runOutsideAngular(() => {
            setTimeout(() => {
                const range = getRange();

                if (range.collapsed) {
                    const startNode = this.getClosestEditableElement(<HTMLElement>getElementNode(range.startContainer));

                    const startTagLength = startNode ? parseFloat((<any>startNode).getAttribute('tag-length')) : 0;

                    if (startTagLength && (range.startOffset > 0 && range.startOffset < +startTagLength)) {
                        range.setStartBefore(range.startContainer);
                        range.collapse(true);
                    }

                    this.findCommonStyle();
                } else {
                    const {startContainer, endContainer, startOffset, endOffset} = range;
                    let startNode = <HTMLElement>getElementNode(startContainer);
                    let endNode = <HTMLElement>getElementNode(endContainer);

                    startNode = this.getClosestEditableElement(startNode);
                    endNode = this.getClosestEditableElement(endNode);

                    const startTagLength = startNode ? parseFloat((<any>startNode).getAttribute('tag-length')) : 0;
                    const endTagLength = endNode ? parseFloat((<any>endNode).getAttribute('tag-length')) : 0;

                    const selection = getSelection();
                    const selRange = getRange().cloneRange();

                    const frontDirection = startContainer === getSelection().anchorNode;

                    if (startTagLength
                        && (startOffset > 0 && startOffset < +startTagLength)
                    ) {
                        const newRange = document.createRange();

                        selRange.collapse();
                        selection.removeAllRanges();

                        if (frontDirection) {
                            newRange.setStart(startContainer, 0);

                            selection.addRange(newRange);
                            selection.extend(endContainer, endOffset);
                        } else {
                            newRange.setStart(endContainer, endOffset);

                            selection.addRange(newRange);
                            selection.extend(range.startContainer, 0);
                        }
                    }

                    if (endTagLength
                        && (endOffset > 0 && endOffset < +endTagLength)
                    ) {
                        const newRange = document.createRange();
                        const lastRange = getRange();

                        selRange.collapse(true);
                        selection.removeAllRanges();

                        if (frontDirection) {
                            newRange.setStart(lastRange.startContainer, lastRange.startOffset);

                            selection.addRange(newRange);
                            selection.extend(endContainer, endTagLength);
                        } else {
                            newRange.setStart(lastRange.endContainer, endTagLength);

                            selection.addRange(newRange);
                            selection.extend(lastRange.startContainer, lastRange.startOffset);
                        }
                    }

                    this.findRangeCommonStyle();
                }
            });
        });
    }

    // Method findCommonStyle runs outside the angular zone
    private findCommonStyle(): void {
        const styles = this.getExtraStyle(getRange().startContainer.parentNode);

        this.zone.run(() => {
            this.currentColor = styles.color;
            this.currentFont = styles.fontFamily;
            this.currentFontSize = styles.fontSize;
        });
    }

    // Method findRangeCommonStyle runs outside the angular zone
    private findRangeCommonStyle(): void {
        const startStyles = this.getExtraStyle(getRange().startContainer.parentNode);
        const endStyles = this.getExtraStyle(getRange().endContainer.parentNode);

        this.zone.run(() => {
            this.currentColor = startStyles.color === endStyles.color ? startStyles.color : '';
            this.currentFont = startStyles.fontFamily === endStyles.fontFamily ? startStyles.fontFamily : '';
            this.currentFontSize = startStyles.fontSize === endStyles.fontSize ? startStyles.fontSize : '';
        });
    }

    private getClosestEditableElement(element: Node | HTMLElement): HTMLElement | null {
        if (!(<HTMLElement>element).classList) {
            return null;
        }

        return (<HTMLElement>element).classList.contains('contenteditable')
            ? <HTMLElement>element
            : <HTMLElement>(<HTMLElement>element).closest('.contenteditable')
        ;
    }

    // Method getExtraStyle runs outside the angular zone
    private getExtraStyle(element: Node): IEditorExtraStyle {
        const {color, fontSize, fontFamily} = window.getComputedStyle(<Element>element);

        return { color, fontSize, fontFamily };
    }

    private isRangeInsideEditor(): boolean {
        const {startContainer, endContainer} = getRange();

        const startNode = startContainer.nodeType === Node.TEXT_NODE ? startContainer.parentNode : startContainer;
        const endNode = endContainer.nodeType === Node.TEXT_NODE ? endContainer.parentNode : endContainer;

        return this.editor.nativeElement.contains(startNode) && this.editor.nativeElement.contains(endNode);
    }
}
