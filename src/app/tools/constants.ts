export const alignPosition = [
    {label: 'left', value: 'justifyLeft', tooltip: 'Left'},
    {label: 'center', value: 'justifyCenter', tooltip: 'Center'},
    {label: 'right', value: 'justifyRight', tooltip: 'Right'}
];

export const fontColors = [
    {label: 'red', value: '#e74011', border: '#af300d', cssValue: 'rgb(231, 64, 17)' },
    {label: 'blue', value: '#1778a0', border: '#1a6894', cssValue: 'rgb(23, 120, 160)' },
    {label: 'green', value: '#86bc25', border: '#5e9248', cssValue: 'rgb(134, 188, 37)' },
    {label: 'grey', value: '#494a4c', border: '#394c60', cssValue: 'rgb(73, 74, 76)' },
    {label: 'black', value: '#000001', border: '#000001', cssValue: 'rgb(0, 0, 0)' }
];

export const fontFamily = [
    {
        title: 'Arial',
        value: 'Arial Helvetica, sans-serif',
        cssValue: 'Arial'
    },
    {
        title: 'Times New Roman',
        value: 'Times New Roman, Times, serif',
        cssValue: '"Times New Roman", Times, serif'
    },
    {
        title: 'Verdana',
        value: 'Verdana, Tahoma, sans-serif',
        cssValue: 'Verdana, Tahoma, sans-serif'
    }
];

export const fontSize = [
    {
        fontValue:  1,
        cssValue: '10px'
    },
    {
        fontValue:  2,
        cssValue: '13px'
    },
    {
        fontValue:  3,
        cssValue: '16px'
    },
    {
        fontValue:  4,
        cssValue: '18px'
    },
    {
        fontValue:  5,
        cssValue: '24px'
    },
    {
        fontValue:  6,
        cssValue: '32px'
    },
    {
        fontValue:  7,
        cssValue: '48px'
    }
];
