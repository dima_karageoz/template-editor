export function isIeBrowser(): boolean {
    return !!(document.all || (!!(<any>window).MSInputMethodContext && !!(<any>document).documentMode));
}

export function getSelection(): Selection {
    if (window.getSelection) {
        return window.getSelection();
    } else if (document.getSelection) {
        return document.getSelection();
    }
}

// export function rangeCollapse(range: Range, toStart: boolean = false): void  {
//     if (isIeBrowser()) {
//         if (toStart) {
//             const selection = getSelection();
//             if (range.startContainer === selection.anchorNode) {
//                 range.setEnd(range.startContainer, range.startOffset);
//             } else {
//                 range.setEnd(range.startContainer, range.startOffset);
//             }
//         }
//     } else {
//         range.collapse(toStart);
//     }
// }

export function getElementNode(fragment: Node): Node {
    return fragment
        ? fragment.nodeType === Node.TEXT_NODE ? fragment.parentNode : fragment
        : fragment
    ;
}

export function isArrowKeyEvent(e: KeyboardEvent): boolean {
    switch (e.key) {
        case 'Down':
        case 'ArrowDown':
        case 'Up':
        case 'ArrowUp':
        case 'Left':
        case 'ArrowLeft':
        case 'Right':
        case 'ArrowRight':
            return true;

        default:
            return false;
    }
}

export function getRange(): Range {
    const selection = getSelection();
    let range: Range;

    if (!selection) {
        return;
    }

    if (typeof selection.getRangeAt === 'function') {
        range = selection.getRangeAt(0);
    } else {
        range = document.createRange();
        range.setStart(selection.anchorNode, selection.anchorOffset);
        range.setEnd(selection.focusNode, selection.focusOffset);
    }

    return range;
}

export function isLeftKey(key: string) {
    return key === 'Left' || key === 'ArrowLeft';
}


export function isRightKey(key: string): boolean {
    return key === 'Right' || key === 'ArrowRight';
}

export function ExtendToSentence (): void {
    if (window.getSelection) {  // all browsers, except IE before version 9
        const selection = window.getSelection();

        if (!selection.isCollapsed) {
            // if the selection end point is within a sentence
            if (selection.focusNode.nodeType === Node.TEXT_NODE) {
                const selRange = selection.getRangeAt (0);
                // if the focus node precedes the anchor node
                if (selRange.startContainer === selection.focusNode && selRange.startOffset === selection.focusOffset) {
                    selRange.setStart (selection.focusNode, 0);
                } else {
                    selRange.setEnd (selection.focusNode, selection.focusNode.textContent.length);
                }

                selection.removeAllRanges ();
                selection.addRange (selRange);
            }
        }
    } else {  // Internet Explorer before version 9
        alert ('Your browser does not support this example');
    }
}

export function selectRangeBackwards(range: Range): void {
    if (typeof window.getSelection !== 'undefined') {
        const sel = window.getSelection();

        if (typeof sel.extend !== 'undefined') {
            const endRange = range.cloneRange();

            endRange.collapse(false);
            sel.removeAllRanges();
            sel.addRange(endRange);
            sel.extend(range.startContainer, range.startOffset);
        }
    }
}
